#include "stdafx.h"
#include <iomanip>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

using std::string;
using std::cout;
using std::cin;
using std::endl;

bool readFile(string fileName, std::vector<string>& output)
{
	if(fileName.size() < 5)
	{
		cout << "Too short name on file " + fileName << endl;
		return false;
	}

	const string fileEnding = fileName.substr(fileName.size()-4, 4);

	if(fileEnding != ".txt")
	{
		cout << "Invalid file ending on file " + fileName << endl;
		return false;
	}

	string line;
	std::ifstream file(fileName);

	if (file.is_open())
	{
		cout << "File " + fileName + " opened." << endl;

		while (getline(file, line))
		{
			output.push_back(line);
		}

		file.close();
		return true;
	}

	cout << "File could not be opened. Are you sure that a file named "
		+ fileName + " exists?" << endl;
	return false;
}

int main()
{
	std::vector<string> originalLines;
	std::vector<string> newLines;

	string firstFile;
	string secondFile;

	cout << "Enter the name of the first file:" << endl;
	cin >> firstFile;

	if (!readFile(firstFile, originalLines))
	{
		cin.get();
		cin.get();
		return 0;
	}

	cout << "Enter the name of the second file:" << endl;
	cin >> secondFile;

	if (!readFile(secondFile, newLines))
	{
		cin.get();
		cin.get();
		return 0;
	}

	std::ofstream diffFile("differences.txt");
	if (diffFile.is_open())
	{
		for (unsigned int i = 0; i < originalLines.size(); i++)
		{
			if (originalLines[i] != newLines[i])
			{
				diffFile << i + 1 << ": " + originalLines[i] + ", " + newLines[i] << endl;
			}
		}

		diffFile.close();
	}
	else
	{
		cout << "Could not create a new file for differences." << endl;
	}

	cout << "Finished reading the files. Read file \"differences.txt\"" << endl;
	cin.get();
	cin.get();
	return 0;
}
